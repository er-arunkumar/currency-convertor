# Currency Converter App

This is a simple Currency Converter App built with React and TailwindCSS.

## Live Demo:

[Currency Converter App](https://currency-convertor-olive.vercel.app/)

## Features

- **Responsive Design:** The app is designed to be responsive, ensuring a seamless user experience across various devices.

- **User Interaction:** Users can input the amount they want to convert and select the currencies they want to convert between.

- **Real-time Conversion:** The app fetches real-time exchange rates from an API and performs currency conversion accordingly.

- **Result Display:** The converted amount is displayed to the user in real-time.

## Technologies Used

- **React:** A JavaScript library for building user interfaces.

- **TailwindCSS:** A utility-first CSS framework for rapid UI development.

- **API Integration:** Utilizes a currency exchange rate API to fetch real-time exchange rates.

## Getting Started

### Prerequisites

- Node.js (v12 or later)
- npm (v6 or later) or yarn

#### Installation
1. Clone the repository:
    ```sh
    git clone https://gitlab.com/er-arunkumar/currency-convertor.git
    cd quiz-app
    ```

2. Install dependencies:
    ```sh
    npm install
    # or
    yarn install
    ```

#### Running Locally
To run the project locally:
```sh
npm start
# or
yarn start
```
#### Author: 

- Arunkumar
