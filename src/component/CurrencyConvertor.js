import axios from 'axios';
import React, { useEffect, useState } from 'react'

export default function CurrencyConvertor() {
    const [currencyData,setCurrencyData] = useState({});
    const [inputCurrency, setInputCurrency] =useState(1);
    const [fromCurrency, setFromCurrency] = useState("USD");
    const [toCurrency, setToCurrency] = useState("INR");
    const [resultAmount, setResultAmount] = useState();

    useEffect(()=>{
        const dataFetch = async()=>{
            try{
                const response =await axios.get(`https://open.er-api.com/v6/latest/${fromCurrency}`);
                console.log(response);
                setCurrencyData(response.data.rates);
                console.log(currencyData);
            }
            catch(error){
                console.error(error);
            }
        }
        dataFetch();
    },[fromCurrency, toCurrency]);

    

    const handleConvert = ()=>{
        const result = (inputCurrency*currencyData[toCurrency]).toFixed(2);
        console.log(result);
        setResultAmount(result);
        console.log(resultAmount);
    }
  return (
    <>
 
   <div class="flex items-center justify-center min-h-screen bg-gradient-to-br from-purple-700 to-indigo-500">
   
    <div class="w-11/12 mx-auto max-w-md p-6 bg-white rounded-lg shadow-md">
    <h1 className='text-xl font-medium text-center text-gray-800 mb-3'>Currency Convertor</h1>
        <label class="block mb-2 text-gray-700" for="amount">Amount:</label>
        <input class="w-full px-3 py-2 mb-4 leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline" id="amount" type="number" value={inputCurrency} onChange={(e)=>setInputCurrency(e.target.value)} />

        <label class="block mb-2 text-gray-700" for="fromCurrency">From Currency:</label>
        <select class="w-full px-3 py-2 mb-4 leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline" id="fromCurrency" value={fromCurrency} onChange={(e)=> setFromCurrency(e.target.value)}>
            {Object.keys(currencyData).map((currency, index)=>(
                <option key={index} value={currency}>{currency}</option>
            ))}
        </select>

        <label class="block mb-2 text-gray-700" for="toCurrency">To Currency:</label>
        <select class="w-full px-3 py-2 mb-4 leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline" id="toCurrency" value={toCurrency} onChange={(e)=> setToCurrency(e.target.value)}>
            {Object.keys(currencyData).map(currency=>(
                <option key={currency} value={currency}>{currency}</option>
            ))}
        </select>

        <button class="w-full px-4 py-2 text-white bg-indigo-600 rounded hover:bg-indigo-700 focus:outline-none" onClick={handleConvert}>Convert Currency</button>
        {resultAmount && (
            <p class="mt-4 text-gray-800 text-center bg-indigo-100 border border-indigo-500 rounded py-5 ">Currency Convert <span className='font-semibold'>{inputCurrency}</span> {fromCurrency} = <span className='font-semibold'>{resultAmount}</span> {toCurrency}</p>
        )}
         <div className='text-center text-gray-500 my-5'>
                    Designed by <a href='https://gitlab.com/er-arunkumar' className='text-medium underline text-gray-800'>Arunkumar Selvam</a>
                </div>
    </div>
   

</div>


    </>
    
  )
}
